using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.src.Models
{
    public sealed class CpfValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            
            double soma = 0;
            double resto;
            string cpf = value.ToString().Replace(".","").Replace("-","");
            
            if(cpf == "00000000000")
            {
                return new ValidationResult("CPF Inválido");
            }
            for (int i = 1; i <= 9;i++)
            {
                try{
                    soma += Int64.Parse(cpf[i - 1].ToString()) * (11 - i);
                }
                catch(Exception e)
                {
                    return new ValidationResult("Cpf inválido");
                }
            }
            resto = (soma * 10) % 11;
            if(resto == 10 || resto == 11)
            {
                resto = 0;
            }
            if(cpf[9].ToString() != resto.ToString())
            {
                return new ValidationResult("Cpf Inválido");
            }
            resto = 0;
            soma = 0;
            for (int i = 1; i <= 10;i++)
            {
                soma += Int64.Parse(cpf[i - 1].ToString()) * (12 - i);
            }
            resto = (soma * 10) % 11;
            if (resto == 10 || resto == 11)
            {
                resto = 0;
            }
            if(cpf[10].ToString() != resto.ToString())
            {
                return new ValidationResult("Cpf Inválido");
            }

            return ValidationResult.Success;
        }
    }
}