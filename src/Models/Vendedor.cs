using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static tech_test_payment_api.src.Models.CpfValidation;

namespace tech_test_payment_api.src.Models
{
    
    public class Vendedor
    {
        public int Id { get; set; }
        [Required]
        [CpfValidation]
        public string Cpf { get; set; } = null!;
        [Required]
        public string Nome { get; set; } = null!;

        public string Email { get; set; }
       
        public string Telefone { get; set; }
    }
}