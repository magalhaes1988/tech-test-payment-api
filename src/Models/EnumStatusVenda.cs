using System.ComponentModel;

namespace tech_test_payment_api.src.Models
{
    public enum EnumStatusVenda
    {
            [Description("Pagamento Aprovado")]
            PagamentoAprovado,
            [Description("Enviado para a Transportadora")]
            EnviadoParaATransportadora,
            [Description("Entregue")]
            Entregue,
            [Description("Cancelada")]
            Cancelada

    }
}