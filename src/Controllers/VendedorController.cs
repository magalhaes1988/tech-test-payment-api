using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Context;
using tech_test_payment_api.src.Models;


namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public VendedorController(DatabaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retorna todos os vendedores
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status200OK)]
      
        public IActionResult TodosVendedores(){

            var vendedores = _context.Vendedores.ToList();
            if (!vendedores.Any())
                return NoContent();
            return Ok(vendedores);
        }

        
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult VendedorPorId(int id){

            var vendedor = _context.Vendedores.Find(id);
            if (vendedor==null)
                return NotFound(new
                {
                    Error = "Vendedor não Encontrado"
                });
            return Ok(vendedor);
        }

        /// <summary>
        /// Adiciona um novo Vendedor ao sistema
        /// </summary>
        /// <param name="vendedor"></param>
        /// <returns>Novo vendedor criado com Id: "vendedor.Id"</returns>
        /// <remarks>
        ///     Exemplo de envio:
        ///     POST /Salvar
        ///     {
        ///         "cpf": "123.456.789-11",
        ///         "nome": "Nome-Exemplo",
        ///         "telefone": "(11) 12345-6789",
        ///         "email": "exemplo@exemplo.com.br"
        ///     }
        ///</remarks>
        ///<response code="201">Novo vendedor criado com Id: {id}</response>
        ///<response code="400">Erro de validação, verifique os campos</response>

        [HttpPost]
        
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        
        
        public IActionResult Salvar([FromBody]Vendedor vendedor){
            if(ModelState.IsValid)
            {
                _context.Vendedores.Add(vendedor);
                _context.SaveChanges();
                return CreatedAtAction(nameof(VendedorPorId), new {id = vendedor.Id }, vendedor);
            }
            return BadRequest(new { Error="Erro de validação, verifique os campos"});
        }


        
        
    }
}