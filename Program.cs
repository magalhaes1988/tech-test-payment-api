using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;
using tech_test_payment_api.src.Context;
using Microsoft.OpenApi.Models;
using System;
using System.Reflection;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DatabaseContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Default")));

builder.Services.AddControllers().ConfigureApiBehaviorOptions(options => 
    { options.SuppressMapClientErrors = true; }).AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Payment API",
        Description = "API de Pagamentos do Desafio da Pottencial",
        Contact = new OpenApiContact
        {
            Name = "Marcelo - Email",
            Email = "magalhaes1988@gmail.com"
        }
    });
    var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(r =>
    {
        r.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment API v1");
        r.RoutePrefix = "api-docs";
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
